import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTableModule } from 'primeng/components/datatable/datatable';

import { PrzepisyComponent } from './przepisy.component';
import { PrzepisyRoutingModule } from './przepisy.routes';

@NgModule({
    imports: [
        CommonModule,
        DataTableModule,
        PrzepisyRoutingModule
    ],
    declarations: [
        PrzepisyComponent
    ]
})
export class PrzepisyModule { }
