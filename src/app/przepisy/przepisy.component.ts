import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
    templateUrl: './przepisy.component.html'
})
export class PrzepisyComponent implements OnInit {
    przepisy$: FirebaseListObservable<any[]>;

    constructor(private db: AngularFireDatabase) {}

    ngOnInit() {
        this.przepisy$ = this.db.list('/lista-przepisow', {
                query: {
                    orderByChild: 'slug'
                }
            });
    }
}
