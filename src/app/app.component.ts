import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import {
    AuthService,
    MessagesService
 } from 'app/services';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    menuItems = {
        all: [
            {
                label: 'Przepisy',
                url: ['przepisy']
            }
        ],
        private: [
            {
                label: 'Przepisy',
                url: ['panel/przepisy']
            },
            {
                label: 'Dodaj przepis',
                url: ['panel/przepis']
            },
            {
                label: 'Składniki',
                url: ['panel/skladniki']
            },
            {
                label: 'Dodaj składnik',
                url: ['panel/skladnik']
            },
            {
                label: 'Kategorie posiłków',
                url: ['panel/kategorie-posilkow']
            },
            {
                label: 'Kategorie składników',
                url: ['panel/kategorie-skladnikow']
            }
        ]
    };

    constructor(
        private router: Router,
        private authService: AuthService,
        private messagesService: MessagesService
    ) { }

    login() {
        this.authService.login()
            .then(auth => {
                this.authService.user.isLogged = true;
                this.authService.user.displayName = auth.user.displayName;
                this.authService.user.emailVerified = auth.user.emailVerified;
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Zalogowano ${auth.user.displayName}`
                });
            });
    }

    logout() {
        this.authService.logout()
            .then(_ => {
                this.authService.user.isLogged = false;
                this.authService.user.displayName = '';
                this.authService.user.emailVerified = false;
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Wylogowano`
                });
                this.router.navigate(['/przepisy']);
            });
    }
}
