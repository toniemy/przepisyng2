import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { MessagesService } from 'app/services';

@Component({
    templateUrl: './kategorie-posilkow.component.html'
})
export class KategoriePosilkowComponent implements OnInit {
    @ViewChild('form') form;
    kategoriePosilkow$: FirebaseListObservable<any[]>;

    constructor(
        private db: AngularFireDatabase,
        private messagesService: MessagesService
    ) {}

    ngOnInit() {
        this.kategoriePosilkow$ = this.db.list('/kategorie-posilkow');
    }

    addKategoriaPosilkow(name: string) {
        this.kategoriePosilkow$
            .push({
                name
            })
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Dodano kategorię ${name}`
                });
                this.form.resetForm();
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Dodawanie kategorii nie powiodło się'
            }));
    }

    updateKategoriaPosilkow(key: string, value: string) {
        this.kategoriePosilkow$
            .update(
                key,
                {
                    name: value
                }
            )
            .then(_ => this.messagesService.replaceMessages({
                severity: 'success',
                summary: 'Zmieniono kategorię posiłków'
            }))
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Zmiana kategorii nie powiodła się'
            }));
    }

    removeKategoriaPosilkow(item) {
        this.kategoriePosilkow$
            .remove(item)
            .then(_ => this.messagesService.replaceMessages({
                severity: 'success',
                summary: `Usunięto kategorię ${item.name}`
            }))
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Usunięcie kategorii nie powiodło się'
            }));
    }
}
