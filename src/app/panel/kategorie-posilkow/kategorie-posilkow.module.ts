import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/components/button/button';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { InplaceModule } from 'primeng/components/inplace/inplace';

import { KategoriePosilkowRoutingModule } from './kategorie-posilkow.routes';
import { KategoriePosilkowComponent } from './kategorie-posilkow.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        DataTableModule,
        InputTextModule,
        InplaceModule,
        KategoriePosilkowRoutingModule
    ],
    declarations: [
        KategoriePosilkowComponent
    ]
})
export class PanelKategoriePosilkowModule { }
