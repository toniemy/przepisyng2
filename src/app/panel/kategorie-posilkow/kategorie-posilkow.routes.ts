import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { KategoriePosilkowComponent } from './kategorie-posilkow.component';

const routes: Routes = [
    {
        path: '',
        component: KategoriePosilkowComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class KategoriePosilkowRoutingModule { }
