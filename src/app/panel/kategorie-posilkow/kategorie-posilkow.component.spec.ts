import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KategoriePosilkowComponent } from './kategorie-posilkow.component';

describe('KategoriePosilkowComponent', () => {
    let component: KategoriePosilkowComponent;
    let fixture: ComponentFixture<KategoriePosilkowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [KategoriePosilkowComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(KategoriePosilkowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
