import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'przepisy',
        pathMatch: 'full'
    },
    {
        path: 'przepis',
        loadChildren: './panel-przepis/przepis.module#PanelPrzepisModule'
    },
    {
        path: 'przepisy',
        loadChildren: './panel-przepisy/przepisy.module#PanelPrzepisyModule'
    },
    {
        path: 'kategorie-posilkow',
        loadChildren: './kategorie-posilkow/kategorie-posilkow.module#PanelKategoriePosilkowModule'
    },
    {
        path: 'kategorie-skladnikow',
        loadChildren: './kategorie-skladnikow/kategorie-skladnikow.module#PanelKategorieSkladnikowModule'
    },
    {
        path: 'skladniki',
        loadChildren: './skladniki/skladniki.module#PanelSkladnikiModule'
    },
    {
        path: 'skladnik',
        loadChildren: './skladnik/skladnik.module#PanelSkladnikModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PanelRoutingModule {}
