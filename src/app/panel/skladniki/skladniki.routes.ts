import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SkladnikiComponent } from './skladniki.component';

const routes: Routes = [
    {
        path: '',
        component: SkladnikiComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SkladnikiRoutingModule { }
