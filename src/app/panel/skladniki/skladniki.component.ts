import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import {
    CommonService,
    MessagesService
} from 'app/services';

@Component({
    templateUrl: './skladniki.component.html'
})
export class SkladnikiComponent implements OnInit {
    kategorieSkladnikow$: FirebaseListObservable<any[]>;
    kategorieSkladnikow;
    skladniki$: FirebaseListObservable<any[]>;
    skladniki;

    constructor(
        private db: AngularFireDatabase,
        private commonService: CommonService,
        private messagesService: MessagesService
    ) { }

    ngOnInit() {
        this.kategorieSkladnikow$ = this.db.list('/kategorie-skladnikow');
        this.skladniki$ = this.db.list('/lista-skladnikow');

        const kategorieStream$ = this.kategorieSkladnikow$
            .map(items => items.map(item => ({
                    label: item.name,
                    value: item.$key
                }))
            );

        const skladnikiStream$ = kategorieStream$
            .switchMap(kategorie => {
                this.kategorieSkladnikow = kategorie;
                return this.skladniki$;
            });

        skladnikiStream$.subscribe(skladniki => {
            skladniki.map(skladnik => {
                skladnik.kategoriaName = this.commonService.findItemByValue(this.kategorieSkladnikow, skladnik.kategoria);
                skladnik.kategoriaName = skladnik.kategoriaName.label;
                skladnik.kalorie = +skladnik.kalorie;
            });
            this.skladniki = skladniki;
        });
    }
}
