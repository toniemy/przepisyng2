import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/components/button/button';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';

import { SkladnikiRoutingModule } from './skladniki.routes';
import { SkladnikiComponent } from './skladniki.component';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        DataTableModule,
        DropdownModule,
        SkladnikiRoutingModule
    ],
    declarations: [
        SkladnikiComponent
    ]
})
export class PanelSkladnikiModule { }
