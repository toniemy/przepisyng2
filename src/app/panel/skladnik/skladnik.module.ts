import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ButtonModule } from 'primeng/components/button/button';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';

import { SkladnikRoutingModule } from './skladnik.routes';
import { SkladnikComponent } from './skladnik.component';
import { SkladnikEdycjaComponent } from './skladnik-edycja/skladnik-edycja.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AutoCompleteModule,
        ButtonModule,
        DropdownModule,
        SkladnikRoutingModule
    ],
    declarations: [
        SkladnikComponent,
        SkladnikEdycjaComponent
    ]
})
export class PanelSkladnikModule { }
