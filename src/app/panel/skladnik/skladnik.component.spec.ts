import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkladnikComponent } from './skladnik.component';

describe('SkladnikComponent', () => {
    let component: SkladnikComponent;
    let fixture: ComponentFixture<SkladnikComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SkladnikComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SkladnikComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
