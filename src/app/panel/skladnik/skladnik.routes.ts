import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SkladnikComponent } from './skladnik.component';
import { SkladnikEdycjaComponent } from './skladnik-edycja/skladnik-edycja.component';

const routes: Routes = [
    {
        path: '',
        component: SkladnikComponent
    },
    {
        path: ':slug',
        component: SkladnikEdycjaComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SkladnikRoutingModule { }
