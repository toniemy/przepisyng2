import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkladnikEdycjaComponent } from './skladnik-edycja.component';

describe('SkladnikEdycjaComponent', () => {
    let component: SkladnikEdycjaComponent;
    let fixture: ComponentFixture<SkladnikEdycjaComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SkladnikEdycjaComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SkladnikEdycjaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
