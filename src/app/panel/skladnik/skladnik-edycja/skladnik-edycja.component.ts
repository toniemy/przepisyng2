import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import 'rxjs/add/operator/map';

import { MessagesService } from 'app/services';

@Component({
    templateUrl: './skladnik-edycja.component.html'
})
export class SkladnikEdycjaComponent implements OnInit {
    slug;
    skladnik$: FirebaseListObservable<any>;
    skladnik;
    kategorieSkladnikow$: FirebaseListObservable<any>;
    listaSkladnikow$: FirebaseListObservable<any>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private db: AngularFireDatabase,
        private messagesService: MessagesService
    ) {
        this.slug = route.params.map(p => p.slug);
    }

    ngOnInit() {
        this.skladnik$ = this.db
            .list('/lista-skladnikow', {
                query: {
                    orderByChild: 'slug',
                    equalTo: this.slug
                }
            });
        this.skladnik$.subscribe(item => this.skladnik = item[0]);

        this.kategorieSkladnikow$ = this.db.list('/kategorie-skladnikow')
            .map(items => items.map(item => ({
                label: item.name,
                value: item.$key
            }))) as FirebaseListObservable<any[]>;
    }

    updateSkladnik(item) {
        this.listaSkladnikow$ = this.db.list('/lista-skladnikow');
        this.listaSkladnikow$
            .update(
                item.$key,
                {
                    nazwa: item.nazwa,
                    slug: item.slug,
                    kategoria: item.kategoria,
                    kalorie: item.kalorie,
                    tluszcz: item.tluszcz,
                    cholesterol: item.cholesterol,
                    sod: item.sod,
                    potas: item.potas,
                    weglowodany: item.weglowodany,
                    bialko: item.bialko
                }
            )
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Zapisano składnik ${item.nazwa}`
                });
                this.router.navigate(['panel/skladniki']);
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Zapisywanie składnika nie powiodło się'
            }));
    }
}
