import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import 'rxjs/add/operator/map';

import { MessagesService } from 'app/services';

@Component({
    templateUrl: './skladnik.component.html'
})
export class SkladnikComponent implements OnInit {
    kategorieSkladnikow$: FirebaseListObservable<any[]>;
    listaSkladnikow$: FirebaseListObservable<any[]>;

    constructor(
        private router: Router,
        private db: AngularFireDatabase,
        private messagesService: MessagesService
    ) { }

    ngOnInit() {
        this.kategorieSkladnikow$ = this.db.list('/kategorie-skladnikow')
            .map(items => items.map(item => ({
                label: item.name,
                value: item.$key
            }))) as FirebaseListObservable<any[]>;
    }

    addSkladnik(item): void {
        this.listaSkladnikow$ = this.db.list('/lista-skladnikow');
        this.listaSkladnikow$
            .push(item)
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Dodano składnik ${item.nazwa}`
                });
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Dodawanie składnika nie powiodło się'
            }));
        this.router.navigate(['panel/skladniki']);
    }
}
