import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KategorieSkladnikowComponent } from './kategorie-skladnikow.component';

describe('KategorieSkladnikowComponent', () => {
    let component: KategorieSkladnikowComponent;
    let fixture: ComponentFixture<KategorieSkladnikowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [KategorieSkladnikowComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(KategorieSkladnikowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
