import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { MessagesService } from 'app/services';

@Component({
    templateUrl: './kategorie-skladnikow.component.html'
})
export class KategorieSkladnikowComponent implements OnInit {
    @ViewChild('form') form;
    kategorieSkladnikow$: FirebaseListObservable<any[]>;

    constructor(
        private db: AngularFireDatabase,
        private messagesService: MessagesService
    ) {}

    ngOnInit() {
        this.kategorieSkladnikow$ = this.db.list('/kategorie-skladnikow');
    }

    addKategoriaSkladnikow(name: string) {
        this.kategorieSkladnikow$
            .push({
                name
            })
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Dodano kategorię ${name}`
                });
                this.form.resetForm();
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Dodawanie kategorii nie powiodło się'
            }));
    }

    updateKategoriaSkladnikow(key: string, value: string) {
        this.kategorieSkladnikow$
            .update(
                key,
                {
                    name: value
                }
            )
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: 'Zmieniono kategorię składników'
                });
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Zmiana kategorii nie powiodła się'
            }));
    }

    removeKategoriaSkladnikow(item) {
        this.kategorieSkladnikow$
            .remove(item)
            .then(_ => this.messagesService.replaceMessages({
                severity: 'success',
                summary: `Usunięto kategorię ${item.name}`
            }))
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Usunięcie kategorii nie powiodło się'
            }));
    }
}
