import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ButtonModule } from 'primeng/components/button/button';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { EditorModule } from 'primeng/components/editor/editor';
import { SharedModule } from 'primeng/components/common/shared';

import { PrzepisRoutingModule } from './przepis.routes';
import { PrzepisComponent } from './przepis.component';
import { PrzepisEdycjaComponent } from './przepis-edycja/przepis-edycja.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AutoCompleteModule,
        ButtonModule,
        DataTableModule,
        DialogModule,
        EditorModule,
        SharedModule,
        PrzepisRoutingModule
    ],
    declarations: [
        PrzepisComponent,
        PrzepisEdycjaComponent
    ]
})
export class PanelPrzepisModule { }
