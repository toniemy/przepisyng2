import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrzepisComponent } from './przepis.component';
import { PrzepisEdycjaComponent } from './przepis-edycja/przepis-edycja.component';

const routes: Routes = [
    {
        path: '',
        component: PrzepisComponent
    },
    {
        path: ':slug',
        component: PrzepisEdycjaComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PrzepisRoutingModule { }
