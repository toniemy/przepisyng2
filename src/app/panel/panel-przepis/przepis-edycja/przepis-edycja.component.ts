import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { reject } from 'lodash';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import {
    CommonService,
    MessagesService
} from 'app/services';

@Component({
    templateUrl: './przepis-edycja.component.html'
})
export class PrzepisEdycjaComponent implements OnInit {
    @ViewChild('form') form;
    slug;
    displayDialog = false;
    kategorieSkladnikow$: FirebaseListObservable<any[]>;
    kategorieSkladnikow;
    skladniki$: FirebaseListObservable<any[]>;
    skladniki;
    przepisy$: FirebaseListObservable<any[]>;
    przepis$: FirebaseListObservable<any>;
    przepis;
    filteredSkladniki;
    selectedSkladnik = {};

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private db: AngularFireDatabase,
        private commonService: CommonService,
        private messagesService: MessagesService
    ) {
        this.slug = route.params.map(p => p.slug);
    }

    ngOnInit() {
        this.kategorieSkladnikow$ = this.db.list('/kategorie-skladnikow');
        this.skladniki$ = this.db.list('/lista-skladnikow');
        this.przepis$ = this.db
            .list('/lista-przepisow', {
                query: {
                    orderByChild: 'slug',
                    equalTo: this.slug
                }
            });

        const kategorieStream$ = this.kategorieSkladnikow$
            .map(items => items.map(item => ({
                    label: item.name,
                    value: item.$key
                }))
            );

        const skladnikiStream$ = kategorieStream$
            .switchMap(kategorie => {
                this.kategorieSkladnikow = kategorie;
                return this.skladniki$;
            });

        const przepisStream$ = skladnikiStream$
            .switchMap(skladniki => {
                skladniki.map(skladnik => {
                    skladnik.kategoriaName = this.commonService.findItemByValue(this.kategorieSkladnikow, skladnik.kategoria);
                    skladnik.kategoriaName = skladnik.kategoriaName.label;
                });
                this.skladniki = skladniki;
                return this.przepis$;
            });

        przepisStream$.subscribe(item => {
            const przepis = item[0];
            if (przepis) {
                przepis.skladnikiKeys.map(skladnik => {
                    skladnik.ilosc = +skladnik.ilosc;
                    skladnik.skladnik = this.commonService.findItemByKey(this.skladniki, skladnik.id);
                });
            }
            this.przepis = przepis;
        });
    }

    updatePrzepis(item: any) {
        this.przepisy$ = this.db.list('/lista-przepisow');
        this.przepisy$
            .update(
                item.$key,
                {
                    nazwa: item.nazwa,
                    slug: item.slug,
                    sposobPrzygotowania: item.sposobPrzygotowania,
                    skladnikiKeys: item.skladnikiKeys
                        .map(skladnik => ({
                            id: skladnik.id,
                            ilosc: skladnik.ilosc,
                            opis: skladnik.opis
                        }))
                }
            )
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Zmieniono przepis ${item.nazwa}`
                });
                this.router.navigate(['panel/przepisy']);
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Zmiana przepisu nie powiodła się'
            }));
    }

    showDialogSkladniki() {
        this.displayDialog = true;
    }

    addOrEditSkladnik(item) {
        this.displayDialog = false;
        const ilosc = item.ilosc ? item.ilosc : 0;
        const opis = item.opis ? item.opis : '';
        const skladnikNew = {
            id: item.skladnik.$key,
            ilosc,
            opis,
            skladnik: item.skladnik
        };
        this.przepis.skladnikiKeys = [...reject(this.przepis.skladnikiKeys, skladnikNew), skladnikNew];
        this.selectedSkladnik = {};
    }

    findSkladnik(event) {
        this.filteredSkladniki = this.commonService.filterList(event.query, this.skladniki);
    }

    removeSkladnik(skladnik) {
        this.przepis.skladnikiKeys = reject(this.przepis.skladnikiKeys, ({id}) => id === skladnik.id);
    }

    editSkladnik(skladnik) {
        this.showDialogSkladniki();
        this.selectedSkladnik = skladnik;
    }
}
