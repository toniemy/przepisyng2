import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrzepisEdycjaComponent } from './przepis-edycja.component';

describe('PrzepisEdycjaComponent', () => {
  let component: PrzepisEdycjaComponent;
  let fixture: ComponentFixture<PrzepisEdycjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrzepisEdycjaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrzepisEdycjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
