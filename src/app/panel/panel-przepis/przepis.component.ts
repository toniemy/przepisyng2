import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import {
    CommonService,
    MessagesService
} from 'app/services';

@Component({
    templateUrl: './przepis.component.html'
})
export class PrzepisComponent implements OnInit {
    @ViewChild('form') form;
    displayDialog = false;
    skladniki$: FirebaseListObservable<any[]>;
    skladniki;
    przepisy$: FirebaseListObservable<any[]>;
    filteredSkladniki;
    addedSkladniki = [];
    selectedSkladnik = {};

    constructor(
        private router: Router,
        private db: AngularFireDatabase,
        private messagesService: MessagesService,
        private commonService: CommonService
    ) { }

    ngOnInit() {
        this.skladniki$ = this.db.list('/lista-skladnikow');
        this.skladniki$.subscribe(items => this.skladniki = items);
        this.przepisy$ = this.db.list('/lista-przepisow');
    }

    addPrzepis(przepis) {
        przepis.skladnikiKeys = this.addedSkladniki
            .map(item => ({
                ilosc: +item.ilosc,
                opis: item.opis,
                id: item.skladnik.id
            }));
        this.przepisy$
            .push({
                nazwa: przepis.nazwa,
                slug: przepis.slug,
                sposobPrzygotowania: przepis.sposobPrzygotowania,
                skladnikiKeys: przepis.skladnikiKeys
            })
            .then(_ => {
                this.messagesService.replaceMessages({
                    severity: 'success',
                    summary: `Dodano przepis ${przepis.nazwa}`
                });
            })
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Dodawanie przepisu nie powiodło się'
            }));
        this.router.navigate(['panel/przepisy']);
    }

    showDialogSkladniki() {
        this.displayDialog = true;
    }

    addSkladnik(item) {
        this.displayDialog = false;
        const ilosc = item.ilosc ? item.ilosc : 0;
        const opis = item.opis ? item.opis : '';
        const {
            bialko,
            cholesterol,
            kalorie,
            kategoria,
            nazwa,
            potas,
            slug,
            sod,
            tluszcz,
            weglowodany
        } = item.skladnik;
        const skladnikNew = {
            ilosc,
            opis,
            skladnik: {
                bialko,
                cholesterol,
                kalorie,
                kategoria,
                nazwa,
                potas,
                slug,
                sod,
                tluszcz,
                weglowodany,
                id: item.skladnik.$key
            }
        };
        this.addedSkladniki = [...this.addedSkladniki, skladnikNew];
        this.selectedSkladnik = {};
    }

    findSkladnik(event) {
        this.filteredSkladniki = this.commonService.filterList(event.query, this.skladniki);
    }
}
