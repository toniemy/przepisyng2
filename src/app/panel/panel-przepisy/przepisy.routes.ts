import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrzepisyComponent } from './przepisy.component';

const routes: Routes = [
    {
        path: '',
        component: PrzepisyComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PrzepisyRoutingModule { }
