import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/components/button/button';
import { DataTableModule } from 'primeng/components/datatable/datatable';

import { PrzepisyRoutingModule } from './przepisy.routes';
import { PrzepisyComponent } from './przepisy.component';

@NgModule({
    imports: [
        CommonModule,
        PrzepisyRoutingModule,
        ButtonModule,
        DataTableModule
    ],
    declarations: [PrzepisyComponent]
})
export class PanelPrzepisyModule { }
