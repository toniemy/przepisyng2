import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { MessagesService } from 'app/services';

@Component({
    templateUrl: './przepisy.component.html'
})
export class PrzepisyComponent implements OnInit {
    przepisy$: FirebaseListObservable<any>;

    constructor(
        private db: AngularFireDatabase,
        private messagesService: MessagesService
    ) {}

    ngOnInit() {
        this.przepisy$ = this.db.list('/lista-przepisow');
    }

    removePrzepis(item) {
        this.przepisy$
            .remove(item)
            .then(_ => this.messagesService.replaceMessages({
                severity: 'success',
                summary: `Usunięto przepis ${item.nazwa}`
            }))
            .catch(_ => this.messagesService.replaceMessages({
                severity: 'error',
                summary: 'Usunięcie przepisu nie powiodło się'
            }));
    }
}
