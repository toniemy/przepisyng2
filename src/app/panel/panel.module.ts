import { NgModule } from '@angular/core';

import { PanelRoutingModule } from './panel.routes';

@NgModule({
    imports: [
        PanelRoutingModule
    ],
    declarations: []
})
export class PanelModule { }
