import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import 'rxjs/add/operator/map';

import {
    CommonService,
    IngredientsService
} from 'app/services';

@Component({
  templateUrl: './przepis.component.html'
})
export class PrzepisComponent implements OnInit {
    przepis$: FirebaseListObservable<any>;
    przepis;
    slug;
    chart;
    weight;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private db: AngularFireDatabase,
        private commonService: CommonService,
        private ingredientsService: IngredientsService
    ) {
        this.slug = route.params.map(p => p.slug);
    }

    ngOnInit() {
        this.przepis$ = this.db
            .list('/lista-przepisow', {
                query: {
                    orderByChild: 'slug',
                    equalTo: this.slug
                }
            });

        this.przepis$.subscribe(przepis => {
            const {id, nazwa, skladnikiKeys, slug, sposobPrzygotowania} = przepis[0];
            this.przepis = {
                nazwa,
                slug,
                sposobPrzygotowania,
                skladnikiSum: {},
                przepisChart: {},
                weight: 0
            };
            this.przepis.skladniki = skladnikiKeys
                .map(item => {
                    this.db
                        .list('/lista-skladnikow', {
                            query: {
                                orderByKey: true,
                                equalTo: item.id
                            }
                        })
                        .subscribe(skladnik => {
                            item.ilosc = +item.ilosc;
                            item.skladnik = skladnik[0];

                            item.skladnikCalculated = this.ingredientsService
                                .calculateIngredient(item.ilosc, item.skladnik);

                            item.skladnikChart = this.ingredientsService
                                .prepareChartData(item.skladnikCalculated);

                            this.przepis.skladnikiSum = this.ingredientsService
                                .sumCalculatedIngredients(this.przepis.skladnikiSum, item.skladnikCalculated);

                            this.chart = this.ingredientsService
                                .prepareChartData(this.przepis.skladnikiSum);

                            this.przepis.weight = this.ingredientsService
                                .sumWeight(this.przepis.skladniki);
                        });
                    return item;
                });
        });
    }
}
