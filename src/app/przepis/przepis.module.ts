import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartModule } from 'primeng/components/chart/chart';
import { DataTableModule } from 'primeng/components/datatable/datatable';

import { PrzepisComponent } from './przepis.component';
import { PrzepisRoutingModule } from './przepis.routes';

@NgModule({
    imports: [
        CommonModule,
        ChartModule,
        DataTableModule,
        PrzepisRoutingModule
    ],
    declarations: [
        PrzepisComponent
    ]
})
export class PrzepisModule { }
