import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrzepisComponent } from './przepis.component';

const routes: Routes = [
    {
        path: ':slug',
        component: PrzepisComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PrzepisRoutingModule { }
