import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/services';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'przepisy',
        pathMatch: 'full'
    },
    {
        path: 'przepisy',
        loadChildren: './przepisy/przepisy.module#PrzepisyModule'
    },
    {
        path: 'przepis',
        loadChildren: './przepis/przepis.module#PrzepisModule'
    },
    {
        path: 'panel',
        loadChildren: './panel/panel.module#PanelModule',
        canActivate: [ AuthGuard ],
        canLoad: [ AuthGuard ]
    },
    {
        path: '**',
        loadChildren: './page-not-found/page-not-found.module#PageNotFoundModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
