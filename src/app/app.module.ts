import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AngularFireModule } from 'angularfire2';
import { firebaseConfig } from './config/firebase-config';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { GrowlModule } from 'primeng/components/growl/growl';
import { TabMenuModule } from 'primeng/components/tabmenu/tabmenu';

import { AppRoutingModule } from './app.routes';
import { AppComponent } from './app.component';
import {
    AuthService,
    AuthGuard,
    CommonService,
    IngredientsService,
    MessagesService
} from 'app/services';

@NgModule({
    imports: [
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        HttpModule,
        GrowlModule,
        TabMenuModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AuthService,
        AuthGuard,
        CommonService,
        IngredientsService,
        MessagesService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
