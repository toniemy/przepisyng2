import { Injectable } from '@angular/core';

@Injectable()
export class MessagesService {
    messages = [];

    replaceMessages(message) {
        this.messages = [message];
    }
}
