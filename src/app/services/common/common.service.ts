import { Injectable } from '@angular/core';

import { find } from 'lodash';

@Injectable()
export class CommonService {
    findItemByValue(list, value) {
        return find(list, ['value', value]);
    }

    findItemByKey(list, key) {
        return find(list, ['$key', key]);
    }

    filterList(query, list: any[]): any[] {
        const filtered: any[] = [];
        for (let i = 0; i < list.length; i++) {
            const item = list[i];
            if (item.nazwa.toLowerCase().indexOf(query.toLowerCase()) === 0) {
                filtered.push(item);
            }
        }
        return filtered;
    }
}
