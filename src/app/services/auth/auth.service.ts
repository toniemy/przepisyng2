import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
    user$: Observable<firebase.User>;
    user = {
        isLogged: false,
        displayName: '',
        emailVerified: false
    };

    constructor(private afAuth: AngularFireAuth) {
        this.user$ = afAuth.authState;
    }

    login() {
        return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }

    logout() {
        return this.afAuth.auth.signOut();
    }
}
