import { Injectable } from '@angular/core';

import { forEach, round } from 'lodash';

@Injectable()
export class IngredientsService {
    sumWeight(weights) {
        let sum = 0;
        weights.map(item => {
            sum = sum + round(item.ilosc, 1);
        });
        return sum;
    }

    calculateSubject(quantity, value) {
        return round(quantity / 100 * value, 1);
    }

    calculateIngredient(quantity, ingredient) {
        return {
            bialko: this.calculateSubject(quantity, ingredient.bialko),
            cholesterol: this.calculateSubject(quantity, ingredient.cholesterol),
            kalorie: this.calculateSubject(quantity, ingredient.kalorie),
            potas: this.calculateSubject(quantity, ingredient.potas),
            sod: this.calculateSubject(quantity, ingredient.sod),
            tluszcz: this.calculateSubject(quantity, ingredient.tluszcz),
            weglowodany: this.calculateSubject(quantity, ingredient.weglowodany)
        };
    }

    sumCalculatedIngredients(sum, ingredient) {
        forEach(ingredient, (value, key) => {
            if (!sum[key]) {
                sum[key] = value;
            } else {
                sum[key] = round(sum[key] + value, 1);
            }
        });
        return sum;
    }

    prepareChartData(skladniki) {
        return {
            labels: [
                'Białko',
                'Cholesterol',
                'Potas',
                'Sód',
                'Tłuszcz',
                'Węglowodany'
            ],
            datasets: [
                {
                    data: [
                        skladniki.bialko,
                        skladniki.cholesterol,
                        skladniki.potas,
                        skladniki.sod,
                        skladniki.tluszcz,
                        skladniki.weglowodany
                    ],
                    backgroundColor: [
                        '#C0F2D2',
                        '#1DB8E8',
                        '#FFDF29',
                        '#FF3E2C',
                        '#BA5000',
                        '#77A600'
                    ]
                }
            ]
        }
    }
}
