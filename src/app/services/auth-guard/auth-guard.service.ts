import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, Router } from '@angular/router';

import { AuthService } from 'app/services';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
    isAdmin: boolean;

    constructor(
        private router: Router,
        private authService: AuthService
    ) {
        this.isAdmin = this.authService.user.isLogged && this.authService.user.isLogged;
    }

    canActivate(): boolean {
        return this.isAdmin;
    }

    canLoad(): boolean {
        if (!this.isAdmin) {
            this.router.navigate(['przepisy']);
        };
        return this.isAdmin;
    }
}
