import { PrzepisyPage } from './app.po';

describe('przepisy App', () => {
    let page: PrzepisyPage;

    beforeEach(() => {
        page = new PrzepisyPage();
    });

    it('should display message saying app works', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('app works!');
    });
});
